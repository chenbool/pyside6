import sys
from PySide6 import QtCore, QtWidgets, QtGui
# from PySide6.QtWidgets import *

class MyWidget(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle('table')

        # 创建布局
        self.layout = QtWidgets.QHBoxLayout(self)

        # 创建 table
        tableW = QtWidgets.QTableWidget()

        # 设置 行 和 列
        tableW.setColumnCount(3)
        tableW.setRowCount(4)

        # 调整 行 和 列
        tableW.resizeRowsToContents()
        # tableW.resizeColumnsToContents()

        # 设置表格 头部
        tableW.setHorizontalHeaderLabels(['姓名', '年龄', '籍贯'])

        # 隐藏 头部
        # tableW.horizontalHeader().setVisible(False)

        # 自适应 宽度
        tableW.horizontalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Stretch)
        # tableW.verticalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Stretch)

        # 设置表格的最小尺寸及行列
        tableW.verticalHeader().setMinimumSectionSize(5)

        # 设置 某列 宽度
        # tableW.horizontalHeader().resizeSection(1, 100)

        # 隐藏 左侧 y轴 标题 序号
        # tableW.verticalHeader().hide()

        # 隐藏边框
        # tableW.setShowGrid(False)

        # 禁止 表格编辑
        tableW.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)

        # 整行 选择 显示
        tableW.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)

        self.layout.addWidget(tableW)

        tableW.setItem(0, 0, QtWidgets.QTableWidgetItem('小明'))
        tableW.setItem(0, 1, QtWidgets.QTableWidgetItem('26'))
        tableW.setItem(0, 2, QtWidgets.QTableWidgetItem('河北'))


        tableW.clicked.connect(self.magic)

    @QtCore.Slot()
    def magic(self, item):
        print(item.row())

if __name__ == "__main__":
    app = QtWidgets.QApplication([])

    widget = MyWidget()
    widget.resize(500, 400)
    widget.show()

    sys.exit(app.exec())