import sys
from PySide6.QtWidgets import QApplication, QMainWindow
import layout.ui_horizontal as MainHorizontal

if __name__ == "__main__":
    app = QApplication(sys.argv)
    mainWindow = QMainWindow()
    ui = MainHorizontal.Ui_MainWindow()
    ui.setupUi(mainWindow)
    # mainWindow.resize(800, 600)
    mainWindow.show()
    sys.exit(app.exec())