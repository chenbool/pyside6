import sys
from PySide6 import QtCore, QtWidgets

class MyWidget(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()

        # 创建 列表
        self.listView = QtWidgets.QListView()
        # 创建 模型
        self.listModel = QtCore.QStringListModel()
        self.list = ['测试列表 1', '测试列表 2', '测试列表 3']
        # 模型关联列表
        self.listModel.setStringList(self.list)
        # 关联模型
        self.listView.setModel(self.listModel)
        # 连接插槽
        self.listView.clicked.connect(self.listClick)

        # 创建水平布局
        self.layout = QtWidgets.QVBoxLayout(self)
        self.layout.addWidget(self.listView)
        self.setLayout(self.layout)

    @QtCore.Slot()
    def listClick(self, item):
        QtWidgets.QMessageBox.information(
            self,
            '列表',
            self.list[item.row()]
        )


if __name__ == "__main__":
    app = QtWidgets.QApplication([])

    widget = MyWidget()
    widget.resize(500, 400)
    widget.show()

    sys.exit(app.exec())