import sys
from PySide6 import QtCore, QtWidgets

class MyWidget(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle('列表')

        # 创建 列表
        self.listWidget = QtWidgets.QListWidget()
        # self.listWidget.resize(300, 150)
        self.listWidget.setWindowTitle('标题')
        self.listWidget.addItem('列表1')


        # 连接插槽
        self.listWidget.clicked.connect(self.listClick)

        # 创建水平布局
        self.layout = QtWidgets.QVBoxLayout(self)
        self.layout.addWidget(self.listWidget)
        self.setLayout(self.layout)

    @QtCore.Slot()
    def listClick(self, index):
        # print(index.row())
        QtWidgets.QMessageBox.information(
            self,
            '列表',
            self.listWidget.item(
                index.row()
            ).text()
        )


if __name__ == "__main__":
    app = QtWidgets.QApplication([])

    widget = MyWidget()
    widget.resize(300, 400)
    widget.show()

    sys.exit(app.exec())