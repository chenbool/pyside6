from PySide6.QtWidgets import QFileDialog, QDialog, QApplication, QMainWindow


class DlgSelectMp3(QMainWindow):
    def __init__(self, parent=None):
        QMainWindow.__init__(self, parent)

    def save_data(self):
        fileNames = []
        fileDialog = QFileDialog(self)
        # getSaveFileName 这是一个方便的静态函数，它将返回用户选择的文件名。该文件不必存在
        fileNames = fileDialog.getSaveFileName(self, '保存文件', '', 'Text files (*.txt);; All files (*)')
        print(fileNames)
        try:
            with open(fileNames[0], 'w', encoding='utf-8') as f:
                f.write('123')
        except:
            pass


if __name__ == "__main__":
    app = QApplication([])
    dlgmw = DlgSelectMp3()
    selectFiles = dlgmw.save_data()
    app.exec_()