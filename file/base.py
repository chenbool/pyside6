import sys
from PySide6 import QtCore, QtWidgets

class MyWidget(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()

        self.setWindowTitle('文件')
        self.button = QtWidgets.QPushButton("点击")
        self.text = QtWidgets.QLabel("Hello World", alignment=QtCore.Qt.AlignCenter)

        self.layout = QtWidgets.QVBoxLayout(self)
        self.layout.addWidget(self.text)
        self.layout.addWidget(self.button)

        # 连接插槽
        self.button.clicked.connect(self.magic)

    @QtCore.Slot()
    def magic(self):
        self.text.setText('点击了')

        fd = QtWidgets.QFileDialog()

        # 选中单个文件
        # res = fd.getOpenFileName()
        # 获取文件url
        # res = fd.getOpenFileUrl()

        # 选中多个文件
        # res = fd.getOpenFileNames()

        # 获取打开文件夹路径
        # res = fd.getExistingDirectoryUrl()

        # 获取选中文件夹
        # res = fd.getExistingDirectory()

        # 保存文件夹
        # res = fd.getSaveFileName()

        # 选择文件后添加后缀
        # fd.setDefaultSuffix(QtWidgets.QFileDialog.AcceptOpen)
        # fd.setDefaultSuffix('json')

        # 选中获取类型文件
        # fd.setFileMode(QtWidgets.QFileDialog.AnyFile)

        # 打开存在的文件
        # fd.setFileMode(QtWidgets.QFileDialog.ExistingFile)
        # fd.setFileMode(QtWidgets.QFileDialog.ExistingFiles)

        # 打开目录
        # fd.setFileMode(QtWidgets.QFileDialog.Directory)

        # 设置目录
        # fd.setDirectory('./')

        # 设置类型
        # fd.setNameFilter('图片文件(*.jpg *.png)')
        # fd.selectNameFilter("Images (*.png *.jpg)")
        # fd.setNameFilters("Text files (*.txt);;Images (*.png *.jpg)")

        # 文件详情
        # fd.setViewMode(QtWidgets.QFileDialog.Detail)
        # fd.setViewMode(QtWidgets.QFileDialog.List)

        res = fd.getOpenFileName()
        print(res)


if __name__ == "__main__":
    app = QtWidgets.QApplication([])

    widget = MyWidget()
    widget.resize(500, 400)
    widget.show()

    sys.exit(app.exec())