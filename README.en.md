# pyside6

#### Description
GUI开发里大名鼎鼎的Qt，C++开发，支持跨平台跨设备。对Python也有完善API支持，如果工作中有快速开发GUI的需求，然后正好后台服务是python的话，那PyQt就是绝配了。
PyQt目前已支持最新的Qt6，网上教程大多还是PyQt5，建议直接学习最新的PyQt6。
官方Qt的亲儿子是PySide，建议直接学习最新的PySide6（支持Qt6）

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
