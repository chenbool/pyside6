# pyside6

#### 介绍
GUI开发里大名鼎鼎的Qt，C++开发，支持跨平台跨设备。对Python也有完善API支持，如果工作中有快速开发GUI的需求，然后正好后台服务是python的话，那PyQt就是绝配了。
PyQt目前已支持最新的Qt6，网上教程大多还是PyQt5，建议直接学习最新的PyQt6。
官方Qt的亲儿子是PySide，建议直接学习最新的PySide6（支持Qt6）

#### 软件架构
软件架构说明


#### 安装教程

1. https://www.jianshu.com/p/5f228e586cfd
2. https://doc.qt.io/qtforpython-6/contents.html
3. https://zhuanlan.zhihu.com/p/469649813

#### 使用说明

1. https://doc.qt.io/qtforpython-6/contents.html
2. https://zhuanlan.zhihu.com/p/469649813

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
